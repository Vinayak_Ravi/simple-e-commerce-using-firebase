package com.example.e_commerce.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.e_commerce.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private TextInputEditText user,password;
    private TextView signup_Button;
    private Button login_button;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        user=(TextInputEditText) findViewById(R.id.user_Name);
        password=(TextInputEditText) findViewById(R.id.password);
        login_button=(Button)findViewById(R.id.login_But);
        signup_Button=(TextView)findViewById(R.id.sign_But);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        String text = "Do not have an Account? Signup";
        SpannableString spannableString = new SpannableString(text);

        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.BLACK);

        spannableString.setSpan(colorSpan,24,30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        signup_Button.setText(spannableString);




        mAuth = FirebaseAuth.getInstance();

        signup_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(i);
            }
        });


        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(user.length()==0){
                    user.setError("Required Mail");
                }else if (password.length()==0){
                    password.setError("Required Password");
                }


                final String loginuser = user.getText().toString();
                String loginpass = password.getText().toString();


                if(!TextUtils.isEmpty(loginuser)&& !TextUtils.isEmpty(loginpass)){
                    progressBar.setVisibility(View.VISIBLE);

                    mAuth.signInWithEmailAndPassword(loginuser,loginpass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()){
                                Toast.makeText(MainActivity.this,"Login Successfull",Toast.LENGTH_LONG).show();
                                sendToStart();

                            }else{



                                String errorMessage = task.getException().getMessage();
                                Toast.makeText(MainActivity.this,"Error: " +errorMessage,Toast.LENGTH_LONG).show();
                            }

                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }


            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();


        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            sendToStart();

        }
    }

    private void sendToStart() {

        Intent i = new Intent(MainActivity.this,LandingPage.class);
        startActivity(i);
        finish();
    }
}