package com.example.e_commerce.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.e_commerce.R;
import com.example.e_commerce.adapter.MarketListAdapter;
import com.example.e_commerce.adapter.ProductsListAdapter;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {

    ProductsListAdapter productList;
    RecyclerView recyclerView;
    private List<LandinglistModel> landinglistModels = new ArrayList<>();
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        recyclerView = findViewById(R.id.productList);
        back = findViewById(R.id.back);

        productList = new ProductsListAdapter(landinglistModels, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductsActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(productList);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




    }
}