package com.example.e_commerce.activity;

public class MarketListMode {
    String Marketname;
    String latitude;
    String longitude;
    String address;
    String image;
    String rating;

    public String getMarketname() {
        return Marketname;
    }

    public void setMarketname(String marketname) {
        Marketname = marketname;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
