package com.example.e_commerce.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.e_commerce.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class AccountSetup extends AppCompatActivity {

    EditText name, door_No, floor, address1, address2;

    private StorageReference mStorageRef;
    private FirebaseAuth firebaseAuth;

    private FirebaseFirestore firebaseFirestore;
    private String user_id;
    Button sumbit;
    private boolean isChanged = false;
    String disable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_setup);

        firebaseAuth = FirebaseAuth.getInstance();
        user_id = firebaseAuth.getCurrentUser().getUid();
        firebaseFirestore = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        disable = getIntent().getStringExtra("disable");


        name = findViewById(R.id.name);
        door_No = findViewById(R.id.door_no);
        floor = findViewById(R.id.floor);
        address1 = findViewById(R.id.address_one);
        address2 = findViewById(R.id.address_two);
        sumbit = findViewById(R.id.save);

        if (disable != null && !disable.equals("")) {
            name.setEnabled(false);
        }


        firebaseFirestore.collection("Users").document(user_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {

                    if (task.getResult().exists()) {

                        String user_name = task.getResult().getString("name");
                        String user_floor = task.getResult().getString("floor");
                        String user_door = task.getResult().getString("door");
                        String user_address2 = task.getResult().getString("address2");
                        String user_address1 = task.getResult().getString("address1");


                        name.setText(user_name);
                        floor.setText(user_floor);
                        door_No.setText(user_door);
                        address2.setText(user_address2);
                        address1.setText(user_address1);


                    } else {
                        // Toast.makeText(Setup.this, "No data", Toast.LENGTH_SHORT).show();

                    }

                } else {


                    String error = task.getException().getMessage();
                    Toast.makeText(AccountSetup.this, "Firebaseretrieve Error" + error, Toast.LENGTH_SHORT).show();
                }

//                progressBar.setVisibility(View.INVISIBLE);
//                account_Update.setEnabled(true);
            }
        });

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProgressDialog dialog = new ProgressDialog(AccountSetup.this);
                dialog.setMessage("Loading...");
                dialog.setCancelable(false);
                dialog.show();

                String user_name = name.getText().toString();
                String door = door_No.getText().toString();
                String floorNo = floor.getText().toString();
                String address_one = address1.getText().toString();
                String address_two = address2.getText().toString();

//                progressBar.setVisibility(View.VISIBLE);


                if (!TextUtils.isEmpty(user_name)) {

                    user_id = firebaseAuth.getCurrentUser().getUid();
                    Log.e("hit", "hit");

                    DocumentReference dbref = firebaseFirestore.collection("Users").document(user_id);
                    Map<String, String> userMap = new HashMap<>();
                    userMap.put("name", user_name);
                    userMap.put("door", door);
                    userMap.put("floor", floorNo);
                    userMap.put("address1", address_one);
                    userMap.put("address2", address_two);
                    dbref.set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            Log.e("suceess", user_id);
                            Toast.makeText(AccountSetup.this, "suceess", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = getSharedPreferences("preference", MODE_PRIVATE).edit();
                            editor.putBoolean("is_registered", true);
                            editor.putString("firstname", user_name);
                            editor.apply();
                            Intent i = new Intent(AccountSetup.this, LandingPage.class);
                            startActivity(i);
                        }
                    });


                }

            }
        });


    }
}