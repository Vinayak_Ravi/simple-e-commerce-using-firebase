package com.example.e_commerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.e_commerce.R;
import com.example.e_commerce.activity.LandinglistModel;

import java.util.List;

public class LandingAdapter extends RecyclerView.Adapter<LandingAdapter.MyViewHolder> {
    private List<LandinglistModel> imageView_models;
//    int size= 0 ;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;

        public MyViewHolder(View view) {
            super(view);


//            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public LandingAdapter(List<LandinglistModel> imageView_models, Context mContext) {
        this.imageView_models = imageView_models;
        this.context = mContext;

    }

    @Override
    public LandingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.landinglistitem, parent, false);

        return new LandingAdapter.MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//
//        }

    }

    @Override
    public int getItemCount() {
        return 20;
    }

}
