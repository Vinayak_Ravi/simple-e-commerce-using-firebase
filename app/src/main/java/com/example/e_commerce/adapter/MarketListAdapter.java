package com.example.e_commerce.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.e_commerce.R;
import com.example.e_commerce.activity.LandinglistModel;
import com.example.e_commerce.activity.MarketListMode;
import com.example.e_commerce.activity.ProductsActivity;

import java.text.DecimalFormat;
import java.util.List;

import static com.example.e_commerce.activity.LandingPage.latitude;
import static com.example.e_commerce.activity.LandingPage.longitude;

public class MarketListAdapter extends RecyclerView.Adapter<MarketListAdapter.MyViewHolder> {

    private List<MarketListMode> taskList;
    public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;
    String name, image, address, rating;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView marketName, offer, rating, address;
        ImageView image;
        CardView steps_card;


        public MyViewHolder(View view) {
            super(view);

            marketName = view.findViewById(R.id.marketName);
            address = view.findViewById(R.id.marketAddress);
            rating = view.findViewById(R.id.rating);
            offer = view.findViewById(R.id.offer);
            image = view.findViewById(R.id.marketImage);
            steps_card = view.findViewById(R.id.steps_card);

        }

    }

    Context context;

    public MarketListAdapter(List<MarketListMode> imageView_models, Context mContext) {
        this.taskList = imageView_models;
        this.context = mContext;

    }


    @Override
    public MarketListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.marketlistitem, parent, false);

        return new MarketListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


//        Toast.makeText(context,,Toast.LENGTH_SHORT).show()
        MarketListMode t = taskList.get(position);


        name = t.getMarketname();
        rating = t.getRating();
        address = t.getAddress();
        image = t.getImage();
        Log.e("lat", String.valueOf(latitude + "  " + longitude));
        Double lat = Double.valueOf(t.getLatitude());
        Double longi = Double.valueOf(t.getLongitude());

        double latDistance = Math.toRadians(latitude - lat);
        double lngDistance = Math.toRadians(longitude - longi);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(lat))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double kilometer = Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c);
        Log.e("km", String.valueOf(kilometer));
//        Toast.makeText(context,String.valueOf(kilometer),Toast.LENGTH_SHORT).show();

        if (kilometer <= 10.00) {

//            if (name != null && rating != null && address != null && image != null && !name.equals("") && !rating.equals("") && !address.equals("") && !image.equals("")) {

                holder.marketName.setText(name);
                holder.rating.setText(rating + "/5");
                holder.address.setText(address);
                Glide.with(context).load(image).into(holder.image);

                Log.e("name", String.valueOf(t.getMarketname()));
                Log.e("iameg", String.valueOf(t.getImage()));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, ProductsActivity.class);
                        context.startActivity(i);
                    }
                });
//            }

        } else {

            holder.steps_card.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }


/*
    public double CalculationByDistance(String StartP, String EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = Double.parseDouble(StartP);
        double lon1 = Double.parseDouble(EndP);
        double lat2 = latitude;
        double lon2 = longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.e("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);


        if ((latitude == lat) && (longitude == longi)) {
            Toast.makeText(context, "Same", Toast.LENGTH_SHORT).show();
        } else {
            double theta = longitude - longi;
            double dist = Math.sin(Math.toRadians(latitude)) * Math.sin(Math.toRadians(lat)) + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
//            if (unit.equals("K")) {
            dist = dist * 1.609344;
            Toast.makeText(context,String.valueOf(dist),Toast.LENGTH_SHORT).show();
//            } else if (unit.equals("N")) {
//                dist = dist * 0.8684;
//            }
//            return (dist);
        }

        return Radius * c;
    }
*/


}
