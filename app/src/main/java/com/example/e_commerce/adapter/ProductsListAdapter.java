package com.example.e_commerce.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_commerce.R;
import com.example.e_commerce.activity.LandinglistModel;

import java.util.List;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.MyViewHolder> {
    private List<LandinglistModel> imageView_models;
//    int size= 0 ;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;

        public MyViewHolder(View view) {
            super(view);


//            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public ProductsListAdapter(List<LandinglistModel> imageView_models, Context mContext) {
        this.imageView_models = imageView_models;
        this.context = mContext;

    }

    @Override
    public ProductsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.productlistitem, parent, false);

        return new ProductsListAdapter.MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//
//        }

    }

    @Override
    public int getItemCount() {
        return 20;
    }

}
