package com.example.e_commerce.roomDB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MarketDAO {

    @Insert(onConflict = REPLACE)
    void insert(MarketDatas marketDatas);

    @Delete
    void delete(MarketDatas marketDatas);

    @Delete
    void reset(List<MarketDatas> marketData);


//    @Query("UPDATE markets SET text = :sText WHERE ID = :sID")
//    void update(int sID, String sText);

    @Query("SELECT * FROM markets")
    List<MarketDatas> getAll();
}
